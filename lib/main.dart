import 'package:firebase_core/firebase_core.dart';
import 'package:motion_notifier/pages/DevicesPage.dart';
import 'package:motion_notifier/pages/NotificationsPage.dart';
import 'firebase_options.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late String text;

  @override
  Widget build(BuildContext context) {
    return mateApp();
  }

  @override
  void initState() {
    FirebaseMessaging.instance.getInitialMessage().then((message) {
      if (message != null) {
        setState(() {});
      }
    });
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      if (message != null) {
        setState(() {});
      }
    });
    this.text = '';
    asyncFunc();
    super.initState();
  }

  bool __isThereCurrentDialogShowing() {
    return ModalRoute.of(context)?.isCurrent != true;
  }

  Widget mateApp() {
    return MaterialApp(
      title: 'Motion Notifier',
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text('Motion Notifier'),
            bottom: TabBar(
              tabs: [
                Tab(text: 'Dispositivos'),
                Tab(text: 'Eventos'),
              ],
            ),
          ),
          drawer: Drawer(
            child: ListView(
              children: [
                Container(
                  padding: EdgeInsets.all(5),
                  child: Text(this.text),
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              DevicesPage(),
              NotificationsPage(),
            ],
          ),
        ),
      ),
    );
  }

  asyncFunc() {
    String texto = 'HOLA';
    oAsyncFunc().then((value) {
      setState(() {
        texto = value ?? texto;
        this.text = texto;
      });
    });
  }

  Future<String?> oAsyncFunc() async {
    final fcmToken = await FirebaseMessaging.instance.getToken();
    print(fcmToken);
    return fcmToken;
  }
}
