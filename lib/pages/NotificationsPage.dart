import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NotificationsPage extends StatefulWidget {
  NotificationsPage({Key? key}) : super(key: key);

  @override
  State<NotificationsPage> createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage>
    with AutomaticKeepAliveClientMixin<NotificationsPage> {
  Query databaseReference =
      FirebaseDatabase.instance.ref().child('notifications/1/');

  @override
  void initState() {
    FirebaseMessaging.onMessage.listen((message) {
      final notification = message.notification;
      setState(() {
        if (__isThereCurrentDialogShowing() == false) {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text(
                  notification?.title ?? 'Alerta de seguridad',
                  textAlign: TextAlign.center,
                ),
                content: Text(
                  notification?.body ?? 'Movimiento detectado',
                  textAlign: TextAlign.center,
                ),
                actions: [
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: Text('CANCEL'),
                  ),
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: Text('OK'),
                  ),
                ],
                actionsAlignment: MainAxisAlignment.spaceAround,
                // actionsPadding: EdgeInsets.symmetric(horizontal: 4.0),
              );
            },
          );
        }
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FirebaseAnimatedList(
      query: databaseReference,
      itemBuilder: (BuildContext context, DataSnapshot snapshot,
          Animation<double> animation, int index) {
        int noti = snapshot.value as int;
        var time = DateTime.fromMillisecondsSinceEpoch(noti);
        var timeF = DateFormat('dd-MM-yyyyhh:mm').format(time);
        var fecha = timeF.substring(0, 10);
        var hora = timeF.substring(10);

        return buildTile(fecha, hora);
      },
    );
  }

  Widget buildTile(String fecha, String hora) {
    return ExpansionTile(
      leading: Icon(Icons.notification_important),
      collapsedIconColor: Color.fromARGB(255, 218, 37, 25),
      iconColor: Color.fromARGB(255, 218, 37, 25),
      title: Text('Alerta de seguridad'),
      subtitle: Text(hora.isNotEmpty ? hora : 'Null'),
      childrenPadding: EdgeInsets.only(left: 20, right: 20, bottom: 10),
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Dispositivo'),
            Text('Dispositivo 1'),
          ],
        ),
        SizedBox(height: 15),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Fecha'),
            Text(fecha),
          ],
        ),
        SizedBox(height: 15),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Hora'),
            Text(hora),
          ],
        ),
      ],
    );
  }

  bool __isThereCurrentDialogShowing() {
    return ModalRoute.of(context)?.isCurrent != true;
  }

  Future _refresh() async {
    setState(() {});
  }

  @override
  bool get wantKeepAlive => true;
}
