import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DevicesPage extends StatefulWidget {
  DevicesPage({Key? key}) : super(key: key);

  @override
  State<DevicesPage> createState() => _DevicesPageState();
}

class _DevicesPageState extends State<DevicesPage>
    with AutomaticKeepAliveClientMixin<DevicesPage> {
  Query databaseReference = FirebaseDatabase.instance.ref().child('devices/');

  @override
  void initState() {
    FirebaseMessaging.onMessage.listen((message) {
      final notification = message.notification;
      print(message);
      setState(() {
        if (__isThereCurrentDialogShowing() == false) {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text(
                  notification?.title ?? 'Alerta de seguridad',
                  textAlign: TextAlign.center,
                ),
                content: Text(
                  notification?.body ?? 'Movimiento detectado',
                  textAlign: TextAlign.center,
                ),
                actions: [
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: Text('CANCEL'),
                  ),
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: Text('OK'),
                  ),
                ],
                actionsAlignment: MainAxisAlignment.spaceAround,
                // actionsPadding: EdgeInsets.symmetric(horizontal: 4.0),
              );
            },
          );
        }
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FirebaseAnimatedList(
                shrinkWrap: true,
                query: databaseReference,
                itemBuilder: (BuildContext context, DataSnapshot snapshot,
                    Animation<double> animation, int index) {
                  Map device = snapshot.value as Map;
                  device['key'] = snapshot.key;
                  return buildTile(device);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildTile(Map device) {
    return ListTile(
      leading: Icon(Icons.developer_board),
      iconColor: device['active']
          ? Color.fromARGB(255, 19, 197, 19)
          : Color.fromARGB(255, 172, 172, 172),
      title: Text(device['name'] ?? 'Null'),
      subtitle: Text(device['active'] ? 'Activo' : 'Desconectado'),
    );
  }

  bool __isThereCurrentDialogShowing() {
    return ModalRoute.of(context)?.isCurrent != true;
  }

  Future _refresh() async {
    setState(() {});
  }

  @override
  bool get wantKeepAlive => true;
}
